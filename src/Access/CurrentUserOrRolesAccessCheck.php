<?php

namespace Drupal\personal_views_access_control\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Checks page access based on the user set in the path or role(s).
 */
class CurrentUserOrRolesAccessCheck extends CurrentUserOrAlternativeCheckBase {

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructs an CurrentUserOrRolesAccessCheck instance.
   *
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_Match
   *   The current route mtach service.
   */
  public function __construct(ResettableStackedRouteMatchInterface $current_route_Match) {
    $this->currentRouteMatch = $current_route_Match;
  }

  /**
   * {@inheritDoc}
   */
  public function executeAlternativeAccessCheck(AccountInterface $account, AccessResultInterface $current_result) {
    $route = $this->currentRouteMatch->getRouteObject();
    $alternativeRoles = $route->getRequirement('_current_user_roles_access_check');

    if ($alternativeRoles === 'none') {
      if (!$current_result->isNeutral()) {
        return AccessResult::forbidden('The current user is not the user concerned by the page.')->cachePerUser();
      }

      return AccessResult::neutral('No user and roles restriction is linked to the page.');
    }

    // Logic aligned with Drupal\user\Access\RoleAccessCheck.
    // It allows to conjunct the roles with OR ('+') or AND (',').
    $explode_and = array_filter(array_map('trim', explode(',', $alternativeRoles)));
    if (count($explode_and) > 1) {
      $diff = array_diff($explode_and, $account->getRoles());
      if (empty($diff)) {
        return AccessResult::allowed()->cachePerUser();
      }
    }
    else {
      $explode_or = array_filter(array_map('trim', explode('+', $alternativeRoles)));
      $intersection = array_intersect($explode_or, $account->getRoles());
      if (!empty($intersection)) {
        return AccessResult::allowed()->cachePerUser();
      }
    }

    // Unlike Drupal\user\Access\RoleAccessCheck, do not give a chance to
    // other access checks, as it is not aligned with the control logic.
    return AccessResult::forbidden('The current user roles do not grant access to the page.')->cachePerUser();
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentRouteMatch() {
    return $this->currentRouteMatch;
  }

}
