<?php

namespace Drupal\personal_views_access_control\Access;

use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Checks page access based on the user set in the path or permission(s).
 */
class CurrentUserOrPermissionAccessCheck extends CurrentUserOrAlternativeCheckBase {

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructs an CurrentUserOrPermissionAccessCheck instance.
   *
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_Match
   *   The current route mtach service.
   */
  public function __construct(ResettableStackedRouteMatchInterface $current_route_Match) {
    $this->currentRouteMatch = $current_route_Match;
  }

  /**
   * {@inheritDoc}
   */
  public function executeAlternativeAccessCheck(AccountInterface $account, AccessResultInterface $current_result) {
    $route = $this->currentRouteMatch->getRouteObject();
    $alternativePermissions = $route->getRequirement('_current_user_permissions_access_check');

    if ($alternativePermissions === 'none') {
      if (!$current_result->isNeutral()) {
        return AccessResult::forbidden('The current user is not the one concerned by the current page.');
      }

      return AccessResult::neutral('No user nor permission restriction is set for the current page.');
    }

    // Logic aligned with Drupal\user\Access\PermissionAccessCheck.
    // It allows to conjunct the permissions with OR ('+') or AND (',').
    $split = explode(',', $alternativePermissions);
    if (count($split) > 1) {
      return AccessResult::allowedIfHasPermissions($account, $split, 'AND');
    }
    else {
      $split = explode('+', $alternativePermissions);
      return AccessResult::allowedIfHasPermissions($account, $split, 'OR');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentRouteMatch() {
    return $this->currentRouteMatch;
  }

}
