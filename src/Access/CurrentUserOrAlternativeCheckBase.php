<?php

namespace Drupal\personal_views_access_control\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;

/**
 * Abstract class providing basis for access control services on current user.
 */
abstract class CurrentUserOrAlternativeCheckBase implements AccessInterface {

  use CurrentUserAccessCheckTrait;

  /**
   * Checks the account access according to the user set in the path.
   *
   * If the user is not given by path or the check is negative,it execute an
   * extra control based on a implementation of the
   * {@see CurrentUserOrAlternativeCheckBase::executeAlternativeAccessCheck()}"
   * method.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which controlling the access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The control result.
   *
   * @see CurrentUserOrAlternativeCheckBase::executeAlternativeAccessCheck()
   */
  public function access(AccountInterface $account) {
    $currentRouteMatch = $this->getCurrentRouteMatch();
    assert($currentRouteMatch instanceof ResettableStackedRouteMatchInterface);

    $currentUserCheckOptions = [
      'user_parameter_name' => $currentRouteMatch->getRouteObject()->getOption('_user_route_parameter_name') ?? 'user',
    ];

    $currentUserCheckResult = $this->currentUserCheckAccess($currentRouteMatch, $account, $currentUserCheckOptions);

    if ($currentUserCheckResult->isAllowed()) {
      return $currentUserCheckResult;
    }

    return $this->executeAlternativeAccessCheck($account, $currentUserCheckResult);

  }

  /**
   * Executes the alternative access check.
   *
   * It is only called if the check result on the current user is "Neutral".
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which controlling the access.
   * @param \Drupal\Core\Access\AccessResultInterface $current_result
   *   The result of the control check computed so far; I.E. on the
   *   current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The alternative control result.
   */
  abstract public function executeAlternativeAccessCheck(AccountInterface $account, AccessResultInterface $current_result);

  /**
   * Gets the current route match service.
   *
   * @return \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   *   The current route match service.
   */
  abstract public function getCurrentRouteMatch();

}
