<?php

namespace Drupal\personal_views_access_control\Access;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides trait controlling access based on the current page path.
 */
trait CurrentUserAccessCheckTrait {

  /**
   * Checks views access based on the user set in the current route match.
   *
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_match
   *   The current route match service allowing to retrieve the user parameter.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which controlling the access.
   * @param array $options
   *   Optional associative array containing the access options.
   *   If set, it must contain the "user_parameter_name" keyed array item.
   *   This item indicates the name of the "user" parameter used in the route.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Returns "allows" if the account matches the user set in the current
   *   route path; otherwise forbidden.
   *   It returns "neutral" if the user is not given by the path.
   */
  public function currentUserCheckAccess(ResettableStackedRouteMatchInterface $current_route_match,
      AccountInterface $account,
      array $options = ['user_parameter_name' => 'user']) {
    $parameterValueName = Html::escape($options['user_parameter_name']);
    $parameterValue = $current_route_match->getRawParameter($parameterValueName);

    if (!is_null($parameterValue)) {
      // Views path can reference multiple ids in a parameter.
      // Let's check each of them. If one of them corresponds to the
      // current user, then, the access is allowed.
      $pageRelatedUsers = array_filter(preg_split('/[,+ ]/', $parameterValue));

      if (in_array($account->id(), $pageRelatedUsers)) {
        return AccessResult::allowed();
      }

      return AccessResult::forbidden('The current user is not the one referenced by the page.');
    }

    return AccessResult::neutral('No user referenced by the page.');
  }

}
