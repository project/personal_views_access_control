<?php

namespace Drupal\personal_views_access_control\Plugin\views\access;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a trait with methods common to every access plugin.
 */
trait CurrentUserOrAlternativeTrait {

  /**
   * Injects the options common to any current user-related access plugins.
   *
   * @param array $options
   *   An associative array containing the options already defined.
   */
  public function defineCommonOptions(array &$options) {
    $options['user_parameter_name'] = ['default' => 'user'];
  }

  /**
   * Builds common part of the options form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The options form state.
   */
  public function buildOptionsFormCommonDefinition(array &$form, FormStateInterface $form_state) {
    $form['user_parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User parameter name used in the path'),
      '#description' => $this->t('Set the parameter name used in the views path for the user id(s). If you use simple "%", use arg_[n] where [n] is the parameter position in the Views path e.g. for "user/custom-view/%", the parameter name is arg_0.'),
      '#default_value' => $this->options['user_parameter_name'],
      '#field_prefix' => '%',
    ];
  }

}
