<?php

namespace Drupal\personal_views_access_control\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\Plugin\views\access\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Component\Utility\Html;
use Drupal\personal_views_access_control\Access\CurrentUserAccessCheckTrait;

/**
 * Access plugin that provides a control based on the path's user or role(s).
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "views_current_user_role",
 *   title = @Translation("Current user or role(s)"),
 *   help = @Translation("Access will be granted to the user given by the Views path or with the specified role(s).")
 * )
 */
class CurrentUserOrRoles extends Role {

  use CurrentUserOrAlternativeTrait;
  use CurrentUserAccessCheckTrait;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructs a CurrentUserOrRoles object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_match
   *   The current route match service.
   */
  public function __construct(array $configuration,
      $plugin_id,
      $plugin_definition,
      RoleStorageInterface $role_storage,
      ResettableStackedRouteMatchInterface $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $role_storage);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function summaryTitle() {
    $count = count($this->options['role']);
    if ($count > 0) {
      return $this->t('The current user or the roles: "%role_part"', ['%role_part' => parent::summaryTitle()]);
    }

    return $this->t('Only the current user');
  }

  /**
   * {@inheritDoc}
   */
  public function access(AccountInterface $account) {
    $currentUserCheckResult = $this->currentUserCheckAccess($this->currentRouteMatch, $account, $this->options);

    if ($currentUserCheckResult->isAllowed()) {
      return $currentUserCheckResult;
    }

    return parent::access($account);
  }

  /**
   * {@inheritDoc}
   */
  public function alterRouteDefinition(Route $route) {
    if (isset($this->options['user_parameter_name']) && ($this->options['user_parameter_name'] !== 'user')) {
      $option_value = Html::escape($this->options['user_parameter_name']);
      $route->setOption('_user_route_parameter_name', $option_value);
    }

    if ($this->options['role']) {
      // "Or"-based logic aligned with the "Role" based access plugin,
      // see Drupal\user\Plugin\views\access\Role.
      $route->setRequirement('_current_user_roles_access_check', (string) implode('+', $this->options['role']));
      return;
    }

    $route->setRequirement('_current_user_roles_access_check', 'none');
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['use_roles'] = ['default' => FALSE];

    $this->defineCommonOptions($options);

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['use_roles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open the views access to roles'),
      '#description' => $this->t('Keep unchecked if the views must be restricted to the only user referenced in the views path.'),
      '#default_value' => $this->options['use_roles'],
    ];

    parent::buildOptionsForm($form, $form_state);

    // Adapt the parent checknoxes field to the current plugin context.
    $form['role']['#title'] = $this->t('Select role(s) to be used as alternative');
    $form['role']['#description'] = $this->t('Choose the authorized roles that users must have if the views path does not target them.');
    $form['role']['#states'] = [
      'visible' => [
        ':input[name="access_options[use_roles]"]' => ['checked' => TRUE],
      ],
    ];

    $this->buildOptionsFormCommonDefinition($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    $use_roles = $form_state->getValue(['access_options', 'use_roles']);

    if (!$use_roles) {
      $form_state->setValue(['access_options', 'role'], []);
      return;
    }

    parent::validateOptionsForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    return Cache::mergeContexts($contexts, ['user']);
  }

}
