<?php

namespace Drupal\personal_views_access_control\Plugin\views\access;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\Plugin\views\access\Permission;
use Drupal\Core\Session\AccountInterface;
use Drupal\personal_views_access_control\Access\CurrentUserAccessCheckTrait;

/**
 * Access providing provides a control based on the path's user or permission.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "views_current_user_permission",
 *   title = @Translation("Current user or Permission"),
 *   help = @Translation("Access will be granted to the user given by the Views path or with the specified permission.")
 * )
 */
class CurrentUserOrPermission extends Permission {

  use CurrentUserOrAlternativeTrait;
  use CurrentUserAccessCheckTrait;

  /**
   * The current route mtach service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructs a CurrentUserOrPermission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_match
   *   The current route match service.
   */
  public function __construct(array $configuration,
      $plugin_id,
      $plugin_definition,
      PermissionHandlerInterface $permission_handler,
      ModuleHandlerInterface $module_handler,
      ResettableStackedRouteMatchInterface $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $permission_handler, $module_handler);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('module_handler'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function summaryTitle() {
    $permissions = $this->permissionHandler->getPermissions();
    if (isset($permissions[$this->options['perm']])) {
      $appendedTitlePart = $permissions[$this->options['perm']]['title'];
      return $this->t('The current user or the permissions "%perm_part"', ['%perm_part' => $appendedTitlePart]);
    }

    return $this->t('Only the current user');
  }

  /**
   * {@inheritDoc}
   */
  public function access(AccountInterface $account) {
    $currentUserCheckResult = $this->currentUserCheckAccess($this->currentRouteMatch, $account, $this->options);
    if ($currentUserCheckResult->isAllowed()) {
      return $currentUserCheckResult;
    }

    return parent::access($account);
  }

  /**
   * {@inheritDoc}
   */
  public function alterRouteDefinition(Route $route) {
    if (isset($this->options['user_parameter_name']) && ($this->options['user_parameter_name'] !== 'user')) {
      $option_value = Html::escape($this->options['user_parameter_name']);
      $route->setOption('_user_route_parameter_name', $option_value);
    }

    $route->setRequirement('_current_user_permissions_access_check', $this->options['perm']);
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['perm'] = ['default' => 'none'];

    $this->defineCommonOptions($options);

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Alter the "perm" input's title & description to
    // align them with the context.
    $form['perm']['#title'] = $this->t('The permission to be used as alternative');
    $form['perm']['#description'] = $this->t('Choose the permission that users must have if the views path does not target them."');

    $newPermInputOptions = ['none' => $this->t('No alternative permission to use')];
    $newPermInputOptions += $form['perm']['#options'];
    $form['perm']['#options'] = $newPermInputOptions;

    $this->buildOptionsFormCommonDefinition($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    return Cache::mergeContexts($contexts, ['user']);
  }

}
