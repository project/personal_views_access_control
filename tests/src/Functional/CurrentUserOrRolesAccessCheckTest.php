<?php

namespace Drupal\Tests\personal_views_access_control\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the access control service based on roles-based fallback.
 *
 * @group personal_views_access_control
 */
class CurrentUserOrRolesAccessCheckTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
  ];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The users used in the different tests.
   *
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected $testUsers = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUsers['authenticated_user'] = $this->createUser([], 'test_authenticated_user');

    $testUser = $this->createUser([], 'test_content_admin');
    $testUser->addRole('test_content_admin');
    $testUser->save();
    $this->testUsers['test_content_admin'] = $testUser;

    $this->testUsers['current_user_player'] = $this->createUser([], 'current_user_player');

    $testUser = $this->createUser([], 'test_user_admin');
    $testUser->addRole('test_user_admin');
    $testUser->save();
    $this->testUsers['test_user_admin'] = $testUser;

    $testUser = $this->createUser([], 'test_user_with_unrelated_role');
    $testUser->addRole('test_user_and_content_admin');
    $testUser->save();
    $this->testUsers['test_user_with_unrelated_role'] = $testUser;

    $testUser = $this->createUser([], 'test_user_with_granted_roles');
    $testUser->addRole('test_content_admin');
    $testUser->addRole('test_user_admin');
    $testUser->save();
    $this->testUsers['test_user_with_granted_roles'] = $testUser;
  }

  /**
   * Test the access of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageAccess() {
    $loggedUser = $this->testUsers['current_user_player'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_no_roles/' . $loggedUser->id() . '/test-current-user');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with at least one granted role.
   */
  public function testOrRoleBasedPageAccess() {
    $pageUser = $this->testUsers['authenticated_user'];
    $loggedUser = $this->testUsers['test_content_admin'];

    $this->drupalLogin($loggedUser);

    $this->drupalGet('user_restricted_or_roles/' . $pageUser->id() . '/test-or-roles');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($pageUser->getDisplayName());
    $this->assertSession()->pageTextNotContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with granted permission.
   */
  public function testAndRoleBasedPageAccess() {
    $pageUser = $this->testUsers['authenticated_user'];
    $loggedUser = $this->testUsers['test_user_with_granted_roles'];

    $this->drupalLogin($loggedUser);

    $this->drupalGet('user_restricted_and_roles/' . $pageUser->id() . '/test-and-roles');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($pageUser->getDisplayName());
    $this->assertSession()->pageTextNotContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with granted permission.
   */
  public function testRoleBasedPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];

    // Authenticated user without roles.
    $loggedUser = $this->testUsers['authenticated_user'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_no_roles/' . $pageUser->id() . '/test-logged-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Authenticated user with wrong role on a "AND" role constraint.
    $loggedUser = $this->testUsers['test_user_with_unrelated_role'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_and_roles/' . $pageUser->id() . '/test-wrong-role-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Authenticated user with wrong role on a "OR" role constraint.
    $loggedUser = $this->testUsers['test_user_with_unrelated_role'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_or_roles/' . $pageUser->id() . '/test-wrong-role-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Authenticated user with a wrong role on the two required.
    $loggedUser = $this->testUsers['test_user_admin'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_and_roles/' . $pageUser->id() . '/test-wrong-role-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();
  }

  /**
   * Tests the access check on anonymous user.
   */
  public function testAnonymousUserPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];
    // Anonymous user with "OR" roles-based check.
    $this->drupalGet('user_restricted_or_roles/' . $pageUser->id() . '/test-anonymous-user-on-or');

    $this->assertSession()->statusCodeEquals(403);

    // Anonymous user with "AND" roles-based check.
    $this->drupalGet('user_restricted_and_roles/' . $pageUser->id() . '/test-anonymous-user-on-and');

    $this->assertSession()->statusCodeEquals(403);

    // Anonymous user without roles-based check.
    $this->drupalGet('user_restricted_no_roles/' . $pageUser->id() . '/test-anonymous-user');

    $this->assertSession()->statusCodeEquals(403);
  }

}
