<?php

namespace Drupal\Tests\personal_views_access_control\Functional\Views\UI;

use Drupal\Tests\views_ui\Functional\UITestBase;

/**
 * Test the configuration UI of the "current user or permissions"-based access.
 *
 * @group personal_views_access_control
 */
class AccessCurrentUserOrPermissionUITest extends UITestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
    'views_ui',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['current_user_or_permission_no_settings_test'];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the access interface for "CurrentUserOrPermission" option.
   */
  public function testCurrentUserOrPermissionConfiguration() {
    $access_url = "admin/structure/views/nojs/display/current_user_or_permission_no_settings_test/page_1/access";
    $this->drupalGet($access_url);
    $this->submitForm(['access[type]' => 'views_current_user_permission'], 'Apply');
    $this->assertSession()->statusCodeEquals(200);

    $access_options_url = "admin/structure/views/nojs/display/current_user_or_permission_no_settings_test/page_1/access_options";
    $this->drupalGet($access_options_url);
    $formEdit = [
      'access_options[perm]' => 'none',
      'access_options[user_parameter_name]' => 'arg_0',
    ];
    $this->submitForm($formEdit, 'Apply');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->statusMessageNotExists('error');

    $this->submitForm([], 'Save');

    $this->assertSession()->statusMessageNotExists('error');

    $entity_type_manager = $this->container->get('entity_type.manager');
    $view = $entity_type_manager->getStorage('view')->load('current_user_or_permission_no_settings_test');
    $display = $view->getDisplay('default');

    $this->assertEquals('none', $display['display_options']['access']['options']['perm']);
    $this->assertEquals('arg_0', $display['display_options']['access']['options']['user_parameter_name']);

    // Test changing access plugin from "Current user or permission" to "none".
    $this->drupalGet($access_url);
    $this->submitForm(['access[type]' => 'none'], 'Apply');
    $this->submitForm([], 'Save');

    $this->assertSession()->statusMessageNotExists('error');

    // Verify that current user or permission options are not set.
    $view = $entity_type_manager->getStorage('view')->load('current_user_or_permission_no_settings_test');
    $display = $view->getDisplay('default');

    $this->assertFalse(isset($display['display_options']['access']['options']['perm']));
    $this->assertFalse(isset($display['display_options']['access']['options']['user_parameter_name']));
  }

}
