<?php

namespace Drupal\Tests\personal_views_access_control\Functional\Views\UI;

use Drupal\Tests\views_ui\Functional\UITestBase;

/**
 * Test the configuration UI of the "current user or permissions"-based access.
 *
 * @group personal_views_access_control
 */
class AccessCurrentUserOrRoleUITest extends UITestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
    'views_ui',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['current_user_or_roles_no_settings_test'];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the access interface for "CurrentUserOrRole" option.
   */
  public function testCurrentUserOrRoleConfiguration() {
    $access_url = "admin/structure/views/nojs/display/current_user_or_roles_no_settings_test/page_1/access";
    $this->drupalGet($access_url);
    $this->submitForm(['access[type]' => 'views_current_user_role'], 'Apply');
    $this->assertSession()->statusCodeEquals(200);

    $access_options_url = "admin/structure/views/nojs/display/current_user_or_roles_no_settings_test/page_1/access_options";
    $this->drupalGet($access_options_url);
    $formEdit = [
      'access_options[use_roles]' => TRUE,
      'access_options[role][test_content_admin]' => 1,
      'access_options[user_parameter_name]' => 'arg_0',
    ];
    $this->submitForm($formEdit, 'Apply');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->statusMessageNotExists('error');

    $this->submitForm([], 'Save');

    $this->assertSession()->statusMessageNotExists('error');

    $entity_type_manager = $this->container->get('entity_type.manager');
    $view = $entity_type_manager->getStorage('view')->load('current_user_or_roles_no_settings_test');
    $display = $view->getDisplay('default');

    $this->assertEquals(TRUE, $display['display_options']['access']['options']['use_roles']);
    $this->assertEquals(['test_content_admin' => 'test_content_admin'], $display['display_options']['access']['options']['role']);
    $this->assertEquals('arg_0', $display['display_options']['access']['options']['user_parameter_name']);

    // Test changing the use_role option to false.
    $this->drupalGet($access_options_url);
    $formEdit = ['access_options[use_roles]' => FALSE];
    $this->submitForm($formEdit, 'Apply');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->statusMessageNotExists('error');

    $this->submitForm([], 'Save');

    $this->assertSession()->statusMessageNotExists('error');

    $view = $entity_type_manager->getStorage('view')->load('current_user_or_roles_no_settings_test');
    $display = $view->getDisplay('default');

    $this->assertEquals(FALSE, $display['display_options']['access']['options']['use_roles']);
    $this->assertEquals([], $display['display_options']['access']['options']['role']);
    $this->assertEquals('arg_0', $display['display_options']['access']['options']['user_parameter_name']);

    // Test changing access plugin from "Current user or role" to "none".
    $this->drupalGet($access_url);
    $this->submitForm(['access[type]' => 'none'], 'Apply');
    $this->submitForm([], 'Save');

    $this->assertSession()->statusMessageNotExists('error');

    // Verify that current user or role options are not set.
    $view = $entity_type_manager->getStorage('view')->load('current_user_or_roles_no_settings_test');
    $display = $view->getDisplay('default');

    $this->assertFalse(isset($display['display_options']['access']['options']['use_roles']));
    $this->assertFalse(isset($display['display_options']['access']['options']['role']));
    $this->assertFalse(isset($display['display_options']['access']['options']['user_parameter_name']));
  }

}
