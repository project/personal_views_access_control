<?php

namespace Drupal\Tests\personal_views_access_control\Functional\Views;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the views access control based on roles-based fallback.
 *
 * @group personal_views_access_control
 */
class CurrentUserOrRolesAccessCheckTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
  ];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The users used in the different tests.
   *
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected $testUsers = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUsers['authenticated_user'] = $this->createUser([], 'test_authenticated_user');

    $testUser = $this->createUser([], 'test_content_admin');
    $testUser->addRole('test_content_admin');
    $testUser->save();
    $this->testUsers['test_content_admin'] = $testUser;

    $this->testUsers['current_user_player'] = $this->createUser([], 'current_user_player');

    $testUser = $this->createUser([], 'test_user_with_unrelated_role');
    $testUser->addRole('test_user_and_content_admin');
    $testUser->save();
    $this->testUsers['test_user_with_unrelated_role'] = $testUser;

    $testUser = $this->createUser([], 'test_user_with_granted_roles');
    $testUser->addRole('test_content_admin');
    $testUser->addRole('test_user_admin');
    $testUser->save();
    $this->testUsers['test_user_with_granted_roles'] = $testUser;
  }

  /**
   * Test the access of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageAccess() {
    $loggedUser = $this->testUsers['current_user_player'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user/' . $loggedUser->id() . '/current-user-without-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('user/' . $loggedUser->id() . '/current-user-or-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test the access denial of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageDenied() {
    $pageUser = $this->testUsers['authenticated_user'];

    $this->drupalLogin($this->testUsers['current_user_player']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    // Access plugin with no permission set.
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Test the access check based on the roles-based fallback.
   */
  public function testRoleFallbackAccess() {
    $pageUser = $this->testUsers['authenticated_user'];

    // User having the right permission.
    $this->drupalLogin($this->testUsers['test_content_admin']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // User having the right role amongst other.
    $this->drupalLogin($this->testUsers['test_user_with_granted_roles']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();

    // User without the right roles.
    $this->drupalLogin($this->testUsers['test_user_with_unrelated_role']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Tests the views access check with anonymous user.
   */
  public function testAnonymousUserPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    // Access plugin with no permission set.
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-roles-plugin-test');

    $this->assertSession()->statusCodeEquals(403);
  }

}
