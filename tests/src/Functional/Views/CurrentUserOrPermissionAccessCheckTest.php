<?php

namespace Drupal\Tests\personal_views_access_control\Functional\Views;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the Views access control based on permissions-based fallback.
 *
 * @group personal_views_access_control
 */
class CurrentUserOrPermissionAccessCheckTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
  ];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The users used in the different tests.
   *
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected $testUsers = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUsers['authenticated_user'] = $this->createUser([], 'test_authenticated_user');
    $this->testUsers['test_content_admin'] = $this->createUser(['administer nodes'], 'test_content_admin');
    $this->testUsers['test_content_admin_view_profile'] = $this->createUser(
        [
          'administer nodes',
          'access user profiles',
        ],
        'test_content_admin_view_profile'
        );
    $this->testUsers['current_user_player'] = $this->createUser([], 'current_user_player');
    $this->testUsers['test_user_admin'] = $this->createUser(
        [
          'administer users',
          'access user profiles',
        ],
        'test_user_admin'
     );
  }

  /**
   * Test the access of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageAccess() {
    $loggedUser = $this->testUsers['current_user_player'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user/' . $loggedUser->id() . '/current-user-without-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('user/' . $loggedUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Test the access denial of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageDenied() {
    $pageUser = $this->testUsers['authenticated_user'];

    $this->drupalLogin($this->testUsers['current_user_player']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    // Access plugin with no permission set.
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Test the access check based on the permission-based fallback.
   */
  public function testPermissionFallbackAccess() {
    $pageUser = $this->testUsers['authenticated_user'];

    // User having the right permission.
    $this->drupalLogin($this->testUsers['test_content_admin']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // User having the right permission amongst other.
    $this->drupalLogin($this->testUsers['test_content_admin_view_profile']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();

    // User without the right permission.
    $this->drupalLogin($this->testUsers['test_user_admin']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Tests the views access check with anonymous user.
   */
  public function testAnonymousUserPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-or-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);

    // Access plugin with no permission set.
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-without-permission-plugin-test');

    $this->assertSession()->statusCodeEquals(403);
  }

}
