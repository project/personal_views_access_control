<?php

namespace Drupal\Tests\personal_views_access_control\Functional\Views;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the Views access control based on "%" path parameter instead of "user".
 *
 * @group personal_views_access_control
 */
class ViewsWithPercentAsUserPathParameterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
  ];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The users used in the different tests.
   *
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected $testUsers = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUsers['authenticated_user'] = $this->createUser([], 'test_authenticated_user');
    $this->testUsers['current_user_player'] = $this->createUser([], 'current_user_player');
    $this->testUsers['test_content_admin'] = $this->createUser(['administer nodes'], 'test_content_admin');
  }

  /**
   * Test the access of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageAccess() {
    $loggedUser = $this->testUsers['current_user_player'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user/' . $loggedUser->id() . '/current-user-simple-percentage-test');

    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Test the access denial of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageDenied() {
    $pageUser = $this->testUsers['authenticated_user'];

    $this->drupalLogin($this->testUsers['current_user_player']);
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-simple-percentage-test');

    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Test the access denial of a user-specific page by the concerned user.
   */
  public function testAnonymousUserPageDenied() {
    $pageUser = $this->testUsers['authenticated_user'];
    $this->drupalGet('user/' . $pageUser->id() . '/current-user-simple-percentage-test');

    $this->assertSession()->statusCodeEquals(403);

  }

}
