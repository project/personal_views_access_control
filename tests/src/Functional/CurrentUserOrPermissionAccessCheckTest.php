<?php

namespace Drupal\Tests\personal_views_access_control\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the access control service based on permissions-based fallback.
 *
 * @group personal_views_access_control
 */
class CurrentUserOrPermissionAccessCheckTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'personal_views_access_control',
    'personal_views_access_control_test',
  ];

  /**
   * The theme used by default in tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The users used in the different tests.
   *
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected $testUsers = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testUsers['authenticated_user'] = $this->createUser([], 'test_authenticated_user');
    $this->testUsers['test_content_admin'] = $this->createUser(['administer nodes'], 'test_content_admin');
    $this->testUsers['test_content_admin_view_profile'] = $this->createUser(
        [
          'administer nodes',
          'access user profiles',
        ],
        'test_content_admin_view_profile'
        );
    $this->testUsers['current_user_player'] = $this->createUser([], 'current_user_player');
    $this->testUsers['test_user_admin'] = $this->createUser(
        [
          'administer users',
          'access user profiles',
        ],
        'test_user_admin'
     );
  }

  /**
   * Test the access of a user-specific page by the concerned user.
   */
  public function testCurrentUserBasedPageAccess() {
    $loggedUser = $this->testUsers['current_user_player'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_no_permissions/' . $loggedUser->id() . '/test-current-user');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with at least one granted permission.
   */
  public function testOrPermissionBasedPageAccess() {
    $pageUser = $this->testUsers['authenticated_user'];
    $loggedUser = $this->testUsers['test_content_admin_view_profile'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_or_permissions/' . $pageUser->id() . '/test-or-permission');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($pageUser->getDisplayName());
    $this->assertSession()->pageTextNotContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with granted permission.
   */
  public function testAndPermissionBasedPageAccess() {
    $pageUser = $this->testUsers['authenticated_user'];
    $loggedUser = $this->testUsers['test_user_admin'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_and_permissions/' . $pageUser->id() . '/test-and-permission');

    $this->assertSession()->statusCodeEquals(200);

    // Let's ensure the user did not get access by mistake.
    $this->assertSession()->pageTextContains($pageUser->getDisplayName());
    $this->assertSession()->pageTextNotContains($loggedUser->getDisplayName());
  }

  /**
   * Tests the access of user with granted permission.
   */
  public function testPermissionBasedPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];

    // Authenticated user without permissions.
    $loggedUser = $this->testUsers['authenticated_user'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_and_permissions/' . $pageUser->id() . '/test-logged-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Authenticated user with wrong permissions.
    $loggedUser = $this->testUsers['test_content_admin'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_or_permissions/' . $pageUser->id() . '/test-wrong-permission-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Authenticated user with a wrong permission on the two required.
    $loggedUser = $this->testUsers['test_content_admin_view_profile'];

    $this->drupalLogin($loggedUser);
    $this->drupalGet('user_restricted_and_permissions/' . $pageUser->id() . '/test-wrong-permission-user');

    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();
  }

  /**
   * Tests the access check with anonymous user.
   */
  public function testAnonymousUserPageDenied() {
    $pageUser = $this->testUsers['current_user_player'];

    // Anonymous user with "AND" permissions-based check.
    $this->drupalGet('user_restricted_and_permissions/' . $pageUser->id() . '/test-anonymous-user');

    $this->assertSession()->statusCodeEquals(403);
    // Anonymous user with "OR" permissions-based check.
    $this->drupalGet('user_restricted_or_permissions/' . $pageUser->id() . '/test-anonymous-user');

    $this->assertSession()->statusCodeEquals(403);

    // Anonymous user without permissions-based check.
    $this->drupalGet('user_restricted_no_permissions/' . $pageUser->id() . '/test-current-user');

    $this->assertSession()->statusCodeEquals(403);
  }

}
