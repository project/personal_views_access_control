<?php

namespace Drupal\personal_views_access_control_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a controller to generate test pages.
 */
class CurrentUserTestsController extends ControllerBase {

  /**
   * Page display contextualized to a user for test purposes.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user used for the page context.
   * @param string $test_type
   *   Key informaing about the test type.
   *
   * @return array
   *   A simple markup text contextualized with the user name.
   */
  public function userSpecificPage(AccountInterface $user, string $test_type = 'current_user_only') {
    return [
      '#markup' => $this->t('This is the page of %username', ['%username' => $user->getDisplayName()]),
    ];
  }

  /**
   * Page title contextualized with a user for test purposes.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user used for the title context.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The title contextualized with the user name.
   */
  public function userSpecificPageTitle(AccountInterface $user) {
    return [
      '#markup' => $this->t('Page of %username', ['%username' => $user->getDisplayName()]),
    ];
  }

  /**
   * Page display generalized for any user for test purposes.
   *
   * @param string $test_type
   *   Key informaing about the test type.
   *
   * @return array
   *   A simple markup text.
   */
  public function userGenericPage(string $test_type = 'current_user_only') {
    return [
      '#markup' => $this->t('This is the page for every user'),
    ];
  }

}
