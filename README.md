# Personal views access control

This module provides two views access plugin restricting the access of a Views
display to the users referenced in the page path or to users having special 
properties:
* "Current user or Permission" granting access to users with a given 
permission if the page path does not referenced them.
* "Current user or role(s)" granting access to users with one of the given 
roles if the page path does not referenced them.

The module offers also to developers two services implementing the same access
check mechanisms on page paths as the two plugins, so that they can use in 
their modules.

## Table of Content

* Requirements
* Installation
* Configuration
* Note for developers


## Requirements 

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install any contributed Drupal module. 
For further information, see 
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.
2. When defining a Views in the Views admin interface, open the "Access" 
options. 
3. In the "Access restriction", select the "Current user or Permission" option
or the "Current user or role(s)" one.
4. Set the settings of the selected option, I.E.,
    * For the " _Current user or Permission_ " plugin:
       * " _The permission to be used as alternative_ ": Select the permission
       to use as alternative access check. Select 
       " _No alternative permission to use_ ", if the views must be restricted 
       to the only concerned user.
       * " _User parameter name used in the path_ ": The default value is 
       correct in 99% of the case, but if you deal with a path using "%" only, 
       you can change it to match the path configuration, see the field 
       description for more information.
    * For the " _Current user or role_ " plugin:
       * " _Open the views access to roles_ ": Keep it unchecked if the views 
       must be restricted to the only concerned user. In this case, the plugin 
       does the same check as " _Current user or Permission_ " with the 
       " _No alternative permission to use_ " selected.
       * " _Select role(s) to be used as alternative_ ": When checking 
       " _Open the views access to roles_ ", the list of existing roles appears
       wherein the role(s) to consider as alternative access check can be 
       chosen.<br /> 
       Note that any user having at least one of the selected roles can then 
       access the Views display.
       * " _User parameter name used in the path_ ": The default value is 
       correct in 99% of the case, but if you deal with a path using "%" only,
       you can change it to match the path configuration, see the field 
       description for more information.

## Note for developers

The module provides also two services the Views access plugins also use.

Route definitions of a module can call one of them if they needs the same 
access control logics.

### "  _access_check.current_user_access_alternative.permissions_ " service
If a route definition needs this service used already by the 
" _Current user or Permission_ " plugin, this definition just has to declare
the requirement parameter " __current_user_permissions_access_check_ ".

The parameter receives the list of permissions to consider. 
The value format is a string wherein the permissions are separated by either a 
comma (if users must have all the listed permissions to get access) or a 
"+" sign (if users must only have one of the listed permissions to get access). 
Set " _none_ " if the service must only check the user.

### " _access_check.current_user_access_alternative.roles_ " service
If a route definition needs this service used already by the 
" _Current user or role(s)_ " plugin, this definition just has to declare the 
requirement parameter " __current_user_roles_access_check_ ".

The parameter receives the list of roles to consider. The value format is a 
string wherein the roles are separated by either a comma (if users must have 
all the listed roles to get access) or a "+" sign (if users must only have one
of the listed roles to get access). 
Set " _none_ " if the service must only check the user.

### Common route option: __user_route_parameter_name_
If required, both services accepts also the " __user_route_parameter_name_ "
option that indicates the name of the path's user parameter to use.
It accepts the same kind of value as the " _User parameter name used in the 
path_ " setting of the Views access plugins. 
